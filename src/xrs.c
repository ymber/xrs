#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <xcb/xcb.h>

struct region {
    int x;
    int y;
    int width;
    int height;
};

xcb_cursor_t set_cursor( xcb_connection_t *con, xcb_screen_t *screen )
{
    xcb_font_t font = xcb_generate_id( con );
    xcb_open_font( con, font, strlen( "cursor" ), "cursor" );
    xcb_cursor_t cursor = xcb_generate_id( con );
    xcb_create_glyph_cursor( con, cursor, font, font, 130, 130 + 1, 0, 0, 0, 65535, 65535, 65535 );

    xcb_close_font( con, font );
    return cursor;
}

int grab_pointer( xcb_connection_t *con, xcb_screen_t *screen, xcb_cursor_t cursor )
{
    uint16_t mask = XCB_EVENT_MASK_BUTTON_PRESS | XCB_EVENT_MASK_BUTTON_RELEASE |
                    XCB_EVENT_MASK_BUTTON_1_MOTION;
    xcb_grab_pointer_cookie_t cookie = xcb_grab_pointer( con, 0, screen->root, mask,
                                       XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC, screen->root, cursor, XCB_CURRENT_TIME );
    xcb_grab_pointer_reply_t *reply;
    int ret = 1;
    if( ( reply = xcb_grab_pointer_reply( con, cookie, NULL ) ) ) {
        if( reply->status == XCB_GRAB_STATUS_SUCCESS ) {
            ret = 0;
        }
        free( reply );
    }
    return ret;
}

struct region select_region( xcb_connection_t *con )
{
    int x1 = -1;
    int y1 = -1;
    int x2 = -1;
    int y2 = -1;

    int done = 0;
    while( !done ) {
        xcb_generic_event_t *event = xcb_wait_for_event( con );
        switch( event->response_type ) {
            case XCB_BUTTON_PRESS: {
                xcb_button_press_event_t *press = ( xcb_button_press_event_t * )event;
                if( press->detail == XCB_BUTTON_INDEX_1 ) {
                    x1 = press->event_x;
                    y1 = press->event_y;
                    printf( "left mouse button pressed at (%i, %i)\n", x1, y1 );
                }
                break;
            }
            case XCB_BUTTON_RELEASE: {
                xcb_button_release_event_t *release = ( xcb_button_release_event_t * )event;
                if( release->detail == XCB_BUTTON_INDEX_1 ) {
                    printf( "left mouse button released at (%i, %i)\n", release->event_x, release->event_y );
                    x2 = release->event_x;
                    y2 = release->event_y;
                }
                done = 1;
                break;
            }
            case XCB_MOTION_NOTIFY: {
                // Draw selected area here.
                xcb_motion_notify_event_t *motion = ( xcb_motion_notify_event_t * )event;
                printf( "left mouse button moved to (%i, %i)\n", motion->event_x, motion->event_y );
                break;
            }
            default:
                printf( "other event\n" );
        }
        free( event );
    }

    struct region r;
    r.x = x1 < x2 ? x1 : x2;
    r.y = y1 < y2 ? y1 : y2;
    r.width = abs( x1 - x2 );
    r.height = abs( y1 - y2 );

    return r;
}

int main( int argc, const char *argv[] )
{
    xcb_connection_t *connection = xcb_connect( NULL, NULL );
    if( xcb_connection_has_error( connection ) ) {
        xcb_disconnect( connection );
        printf( "error establishing connection\n" );
        return 1;
    }
    const xcb_setup_t *setup = xcb_get_setup( connection );
    xcb_screen_iterator_t iter = xcb_setup_roots_iterator( setup );
    xcb_screen_t *screen = iter.data;

    xcb_cursor_t cursor = set_cursor( connection, screen );
    if( grab_pointer( connection, screen, cursor ) ) {
        printf( "failed to grab pointer\n" );
        return 1;
    }
    struct region rs = select_region( connection );
    xcb_ungrab_pointer( connection, XCB_CURRENT_TIME );
    xcb_free_cursor( connection, cursor );

    printf( "%ix%i+%i+%i\n", rs.width, rs.height, rs.x, rs.y );

    xcb_disconnect( connection );

    return 0;
}
