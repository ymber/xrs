CC=gcc
INCLUDES=`pkg-config --cflags xcb`
LIBS=`pkg-config --libs xcb`
DBG_CFLAGS=-g -O0
REL_CFLAGS=-O3

SOURCES=$(wildcard src/*.c)
EXECUTABLE=xrs

debug:
	$(CC) $(DBG_CFLAGS) $(SOURCES) -o $(EXECUTABLE) $(LIBS)

release:
	$(CC) $(REL_CFLAGS) $(SOURCES) -o $(EXECUTABLE) $(LIBS)

clean:
	rm xrs
